# NIM GAME AGAINST COMPUTER


You can start the game app that links by running

```bash 
go run main.go
```

or you can run the main executable *.exe directly
```bash 
v1.exe
```

##  About

For this challenge had to make the standard variation of the game 'Nim'

The demonstration of this was given : [Demonstration](https://www.archimedes-lab.org/game_nim/play_nim_game.html)

##  Constraints
- The program should have a GUI (you may use a library of your choice) and be written using go.
- The computer should always win if it goes first.
- It should compile to windows.


##  Library
- For this challenge, I decided to use [fyne](https://github.com/fyne-io/fyne)
- They have a simple GUI and the interfaces are easy to use. 
- Fyne can be used to create UIs for all desktops and mobile devices.

# Understanding NIM

[Nim](https://en.wikipedia.org/wiki/Nim) is a game of strategy where two players remove objects from heaps. 

There are multiple variations of this game but the typically played is the misère game.

For this version, to win the game but not being the last person to from the heap. 

## Mathematics Theory Behind Nim

- The key to understanding this game depends on what is called the NIM-SUM
- Nim sum is the xor addition of each heap size
- To win this game you have to no pick the last item and this can be calculated using a NUM-SUM
    - To achieve this you have to make sure every move you make results in a nim-sum of 0
    - In this app, you will be playing again the computer that will follow this rule for every play
    - If the rule is followed (based on the heap arrangement) you can always win if you start first
- To understand the whole theory visit [here](https://en.wikipedia.org/wiki/Nim)

# Understanding my app

This app was designed to allow users to play a game of nim with a computer using the simple GUI. 

![foo](https://bitbucket.org/tobenna_wes/nimgame/raw/01aa102151103e2ca10fecafb56cdde2a3c36d8c/demoImages/firstPage.png)


This app was designed with the capabilities of allowing different game modes and with condition that if the 
the computer started first it the user would always loose

This app has 4 difficulty levels 
- Easy: { 1, 3, 5}

- Medium: {3, 5, 7}

- Hard: {1, 2, 5, 7}

- Legendary: {1, 3, 5, 7, 9}

You can also change the Images of each match stick


# Software Design 

There are two main components of this app

## The UI Component

For the stick that the user taps on we had to create a custom widget 
A widget in fyne is simply a canvas object which used the interface shown below 

```go
type Widget interface {
    CanvasObject
    CreateRenderer() WidgetRenderer
}
```

So for this, I had to create both a Canvas object and a WidgetRenderer
- This was created for both the match stick and the board its self
    - matchStick

    - game

- For this app, we added a few more items to the custom widgets created as you can see bellow

```go
type matchStick struct {
    widget.BaseWidget
    image    *canvas.Image
    circle   *canvas.Circle
    OnTapped func()
}

// the matchStick WidgetRenderer

type matchRenderer struct {
    image     *canvas.Image
    matchItem *canvas.Circle

    objects []fyne.CanvasObject
    match   *matchStick
}
```

I gave the widget an image object so we could show the match stick and more

## The Game Logic Component

This is the driver of the game and contains all the rules needed

```go
type gameLogic struct {
    dimension      []int    //Contains sizes of rows even after Restart
    tracker        []int    //Row data used during the game
    win, lose      func()
    activeRow      int
    isComputerMove bool     //Tracks players turn
}
```


Functions used to allow easy understanding and debugging

```go
func (g *gameLogic) calculateNimSum() (bool, int)

func (g *gameLogic) removeCountToWin(currentRow int) int

func (g *gameLogic) getNextMove(currentRow int) (count, index int)

func (g *gameLogic) getNextValidRow(lastRow int) int

func (g *gameLogic) removePiece(row, count int) 


```


In this app, I applied recursive functions to go through the rows and identify the next move that would lead to a num sum. 

Once the user plays, removePiece() is called and this modifies the game tracker array. 

Once computerMove is called 

- The app looks for a valid row (a row with an item in it) and checks whether the board is a num sum already 

- If it is a num sum this means the user is currently winning so it just removes an Item from the next valid row

- If it is not a num sum it looks for the next possible move to allow for a num sum

- It does this by going through the heap, calculating if removing from a row can cause a num sum

- Once it finds this it removes how many items from that row that lead to a num sum

- then the process starts again

I chose this method because it is easy to understand, debug and implement

For more question please email me at toennaw@gmail.com





