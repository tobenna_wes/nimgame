package main

import (
	"fmt"
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

///////
////// For this game, we have to make my custom widget
//////

type game struct {
	widget.BaseWidget
	gameLogic *gameLogic

	pieces *fyne.Container
	window fyne.Window
}

//Initial Board dimensions used
//For the computer win the formation of the board must always sum up to an odd number

var easyDimension = []int{1, 3, 5}
var medDimension = []int{3, 5, 7}
var hardDimension = []int{1, 2, 5, 7}
var legendaryDimension = []int{1, 3, 5, 7, 9}

//File names of each image used
const matchImageName = "matchImage.png"
const TeslaLogoImageName = "teslaLogo.png"
const teslaBatteryImageName = "teslaBattery.png"

var currentmatchImage = canvas.NewImageFromFile(matchImageName)

func startGame() *game {

	gameLogicItem := &gameLogic{dimension: hardDimension}

	newGame := &game{gameLogic: gameLogicItem}

	newGame.ExtendBaseWidget(newGame)
	return newGame

}

func (g *game) changeMatchImage(name string) {
	//print(name)
	currentmatchImage = canvas.NewImageFromFile(name)
	g.reloadGame(true)
}

//I added this function so we can change the difficulty of the game
func (g *game) changeDifficulty(dim []int) {
	g.gameLogic.dimension = dim
	fmt.Print(dim)
	g.CreateRenderer()
	g.Refresh()

	g.reloadGame(true)
}

//This is in charge of creating our board interface based of the game dimensions
func (g *game) CreateRenderer() fyne.WidgetRenderer {

	//println("CreateRenderer() fyne.WidgetRenderer")
	gridLayout := g.gameLogic.dimension
	var firstLayerGrid *fyne.Container

	layerArray := container.NewVBox()

	for x, item := range gridLayout {

		var layers []fyne.CanvasObject
		layers = append(layers, layout.NewSpacer())

		for y := 0; y < item; y++ {
			row, index := x, y

			matchItemReal := newMatchButton(func() {

				g.removePiece(row, index)

			})
			layers = append(layers, matchItemReal)
		}

		layers = append(layers, layout.NewSpacer())
		firstLayerGrid = container.New(layout.NewHBoxLayout(),
			layers...)

		layerArray.Add(firstLayerGrid)

	}
	height := float32(len(gridLayout)*int(firstLayerGrid.Size().Height)) + 100
	layerArray.Resize(fyne.NewSize(800, height))

	renderer := &gameRenderer{game: g}
	renderer.pieces = layerArray
	g.pieces = renderer.pieces
	return renderer
}

//This function removes pieces from the board interface
func (g *game) removePiece(row, count int) {

	if g.gameLogic.activeRow == len(g.gameLogic.dimension) || g.gameLogic.activeRow == row {
		g.gameLogic.removePiece(row, 1)
		g.gameLogic.isComputerMove = true
		g.gameLogic.activeRow = row
		matchstickClicked := g.pieces.Objects[row].(*fyne.Container)
		stick := matchstickClicked.Objects[count+1].(*matchStick)

		stick.Hidden = true

		stick.Refresh()
	} else {
		g.oneRowReminder()
	}

}

//This is the function clicked on by the user so the computer can make its move
func (g *game) computerMove() {

	//checks if it is the computers turn
	if g.gameLogic.isComputerMove {
		g.gameLogic.activeRow = len(g.gameLogic.dimension)
		removeCount, row := g.gameLogic.computerMove()
		g.gameLogic.removePiece(row, removeCount)
		g.gameLogic.isComputerMove = false

		for i := 0; i < removeCount; i++ {
			matchstickClicked := g.pieces.Objects[row].(*fyne.Container)

			for _, item := range matchstickClicked.Objects[1:] {

				if item.Visible() {
					item.Hide()
					break
				}
			}

			matchstickClicked.Refresh()
		}
	} else {
		g.yourTurn()
	}
}

func (g *game) yourTurn() {
	dialog.ShowInformation("Your Turn", "The computer just made its move, \n it is now your turn", g.window)
}

//Displays a dialog that reminds the user that is one can only select from one row at a time
func (g *game) oneRowReminder() {
	dialog.ShowInformation("You can only select from one row", "Based off the rules you can only remove sticks from one row at a time", g.window)
}

//Displays dialog that shows user won and asks to restart
func (g *game) win() {
	dialog.ShowConfirm("You won! :{)", "Congratulations! \n Would you like play again?",
		g.reloadGame, g.window)
}

//Displays dialog that shows user lost and asks to restart
func (g *game) lose() {
	dialog.ShowConfirm("You lost! :{(", "The computer won the game, \n  Would you like play again?",
		g.reloadGame, g.window)
}

//Displays dialog that shows game rules
func (g *game) instructions() {
	dialog.ShowInformation(" How to Play!", "In one move, you can remove any number of matches, but only from one row\n"+
		"Select an image to REMOVE that match. After each move click 'PC move' to make the computer play.\n"+
		"You win if you leave the LAST match for the computer. Anyone can move first. To have the computer go first, click on 'PC Move'",
		g.window)
}

//Reload game
func (g *game) reloadGame(yes bool) {
	if !yes {
		g.window.Close()
		return
	}

	g.gameLogic.loadGame()
	g.refreshGame()
	g.window.Content().Refresh()
}

//Shows every piece in that game and sets image
func (g *game) refreshGame() {

	for i := range g.gameLogic.dimension {
		matchstickClicked := g.pieces.Objects[i].(*fyne.Container)
		len := len(matchstickClicked.Objects)
		for _, item := range matchstickClicked.Objects[1 : len-1] {
			stick := item.(*matchStick)
			stick.Show()
			stick.SetImage(currentmatchImage)
			stick.Refresh()
		}
	}

}

type gameRenderer struct {
	pieces *fyne.Container
	game   *game
}

///////
////// This is the interface for a renderer used by the fyne library
////// I had to create one so I can render the board

func (g *gameRenderer) Destroy() {
}

func (g *gameRenderer) MinSize() fyne.Size {
	//return fyne.NewSize(800, 800)
	return g.pieces.Size()
}

func (g *gameRenderer) Layout(size fyne.Size) {

}

func (g *gameRenderer) ApplyTheme() {
}

func (g *gameRenderer) BackgroundColor() color.Color {
	return theme.BackgroundColor()
}

func (g *gameRenderer) Refresh() {

	g.pieces = g.game.pieces
	canvas.Refresh(g.pieces)
}

func (g *gameRenderer) Objects() []fyne.CanvasObject {
	return []fyne.CanvasObject{g.pieces}
}
