package main

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

type matchStick struct {
	widget.BaseWidget
	image    *canvas.Image
	circle   *canvas.Circle
	OnTapped func()
}

///////
////// For this game, we have to make a custom widget
//////

func newMatchButton(tapped func()) *matchStick {

	circle := &canvas.Circle{StrokeColor: color.NRGBA{0xff, 0xff, 0xff, 0x00},
		FillColor:   color.NRGBA{0xff, 0x00, 0x00, 0x00},
		StrokeWidth: 1}
	//currentmatchImage = canvas.NewImageFromFile(matchImageName)
	button := &matchStick{
		OnTapped: tapped,
		circle:   circle,
		image:    currentmatchImage,
	}

	button.ExtendBaseWidget(button)
	return button

}

// Tapped is called when a regular tap is reported
func (m *matchStick) Tapped(ev *fyne.PointEvent) {
	//print("Tapped")
	m.OnTapped()
}

// TappedSecondary is called when an alternative tap is reported
func (m *matchStick) TappedSecondary(ev *fyne.PointEvent) {
	m.OnTapped()
}

//This creates the render used do display the widget
//Create Render is part of the interface for a custom widget
func (m *matchStick) CreateRenderer() fyne.WidgetRenderer {

	image := m.image
	image.FillMode = canvas.ImageFillContain

	objects := []fyne.CanvasObject{
		image,
		m.circle,
	}

	return &matchRenderer{image, m.circle, objects, m}
}

// SetIcon updates the icon on a label - pass nil to hide an icon
func (m *matchStick) SetImage(image *canvas.Image) {
	m.image.File = image.File
	m.Refresh()
}

type matchRenderer struct {
	image     *canvas.Image
	matchItem *canvas.Circle

	objects []fyne.CanvasObject
	match   *matchStick
}

///////
////// This is the interface for a renderer used by fyne library
////// Needed to create a costum widget.

const matchSize = 60

// MinSize calculates the minimum size of a bug button. A fixed amount.
func (m *matchRenderer) MinSize() fyne.Size {
	return fyne.NewSize(matchSize+theme.Padding()*2, (matchSize*1.3)+theme.Padding()*2)
}

// Layout the components of the widget
func (m *matchRenderer) Layout(size fyne.Size) {
	inner := size.Subtract(fyne.NewSize(theme.Padding()*2, theme.Padding()*2))
	m.image.Resize(inner)
	m.image.Move(fyne.NewPos(theme.Padding(), theme.Padding()))

	m.matchItem.Resize(size)
	m.image.Move(fyne.NewPos(theme.Padding(), theme.Padding()))
}

// ApplyTheme is called when the bugButton may need to update it's look
func (m *matchRenderer) ApplyTheme() {
	m.Refresh()
}

func (m *matchRenderer) BackgroundColor() color.Color {
	return theme.ButtonColor()
}

func (m *matchRenderer) Refresh() {
	m.Layout(m.match.Size())
	canvas.Refresh(m.match)
}

func (m *matchRenderer) Objects() []fyne.CanvasObject {
	return m.objects
}

func (m *matchRenderer) Destroy() {
}
