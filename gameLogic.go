package main

import (
	"math/rand"
)

type gameLogic struct {
	dimension      []int //Contains sizes of rows even after Restart
	tracker        []int //Row data used during the game
	win, lose      func()
	activeRow      int
	isComputerMove bool //Tracks players turn
}

//Inputs all needed initial parameters
func (g *gameLogic) loadGame() {

	g.tracker = []int{}
	g.activeRow = len(g.dimension)
	g.isComputerMove = true
	if g.dimension == nil {
		g.dimension = []int{}
		g.tracker = []int{}
	} else {
		g.tracker = make([]int, len(g.dimension))
		for x, item := range g.dimension {
			g.tracker[x] = item
		}
	}
}

//Function that Removes pieces from the Board and Checks if the game has been won
func (g *gameLogic) removePiece(row, count int) {

	//Reduce pieces from the board
	g.tracker[row] = g.tracker[row] - count

	//Count how many pieces are on the board
	counter := 0
	for _, item := range g.tracker {
		counter += item
	}

	if counter == 0 && !g.isComputerMove {
		//if user takes the last piece then user has LOST
		g.lose()
	} else if counter == 0 && g.isComputerMove {
		//if there is none left and it is the computers turn user has WON
		g.win()
	}

}

// func (g *gameLogic) computerCanWin() bool {
//  canWin, _ := g.calculateNimSum()
//  return canWin
// }

//return a the next row that has a match in it
//Should not be called after a game is over
func (g *gameLogic) getNextValidRow(lastRow int) int {
	randomPosition := ((lastRow + 1) % len(g.tracker))

	if g.tracker[randomPosition] != 0 {
		return randomPosition
	}
	return g.getNextValidRow(randomPosition)
	//Can be iterative but is easier to debug
	//Might possible take a little more memory

}

//calculates the nim sum of every row on the board
//Nim-Sum: The cumulative XOR value of the number of items
//Can be used to determine how to win the game
//Also Returns a Valid Next Position
func (g *gameLogic) calculateNimSum() (bool, int) {

	nimSum := 0
	isNextMoveGood := false
	isNextMoveGoodTemp := 0
	maxPostion := 0 //max
	minPostion := len(g.tracker)
	for i, item := range g.tracker {
		nimSum ^= item
		isNextMoveGoodTemp |= item
		if i >= maxPostion {
			maxPostion = i
		}
		if i < minPostion {
			minPostion = i
		}
	}

	//This Checks if all rows have only one piece
	//If all rows have one piece then we can win the game
	//needed for the misère game. Which is what is normally played
	isNextMoveGood = isNextMoveGoodTemp == 1

	//A 0 num-sum is needed to win the game
	//Checks if num-sum is Zero
	isNimSum := nimSum == 0

	computerCanWin := !(isNextMoveGood == isNimSum)
	randomPosition := rand.Intn(maxPostion-minPostion) + minPostion
	nextPosition := g.getNextValidRow(randomPosition)

	return computerCanWin, nextPosition

}

//Determines how much needs to be removed from a row to make the game a num sum
func (g *gameLogic) removeCountToWin(currentRow int) int {
	tempVal := g.tracker[currentRow]
	validforWin := false
	removeCount := 0

	for !validforWin && g.tracker[currentRow] > 0 {

		g.tracker[currentRow] -= 1
		removeCount += 1
		validforWin, _ = g.calculateNimSum()

	}

	g.tracker[currentRow] = tempVal
	if !validforWin {
		return 0
	}

	return removeCount
}

//Determines next best move for by checking each row
func (g *gameLogic) getNextMove(currentRow int) (count, index int) {

	removeCount := g.removeCountToWin(currentRow)

	if removeCount == 0 {
		nextRow := g.getNextValidRow(currentRow)
		return g.getNextMove(nextRow)
	} else {
		return removeCount, currentRow
	}

}

//Call by user click
func (g *gameLogic) computerMove() (count, index int) {

	//Checks if computer can win  with first play else gets next valid move to cause a win
	notWinning, validRow := g.calculateNimSum()

	if notWinning {
		return 1, validRow
	} else {
		return g.getNextMove(validRow)
	}

}
