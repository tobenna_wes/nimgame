package main

import (
	"net/url"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("PLAY NIM GAME WITH COMPUTER")
	w.SetMaster() // Sets window that closes app when dismissed

	//Menu SubMenu that is added to the Main menu
	aboutMenu := fyne.NewMenu("About Me",
		fyne.NewMenuItem("View LinkedIn", func() {
			u, _ := url.Parse("https://www.linkedin.com/in/tobennawes/")
			_ = a.OpenURL(u)

		}),

		fyne.NewMenuItem("View Website", func() {
			u, _ := url.Parse("https://www.tobennawes.com/")
			_ = a.OpenURL(u)

		}))

	//Info Labels
	welcomeLabel := widget.NewLabelWithStyle("Play Nim against your computer!", fyne.TextAlignCenter, fyne.TextStyle{Bold: true})
	instructionLabel := widget.NewLabelWithStyle("At the start of a game, you have the first move, \n unless you allow the computer to play first by pressing the 'PC move' button.",
		fyne.TextAlignCenter, fyne.TextStyle{Bold: false})

	InfoContainter := container.NewVBox(welcomeLabel, instructionLabel)

	//initialization of the game
	boardView := startGame()
	boardView.window = w // this window is added to this struct so we can call pop up menu funcition
	boardView.gameLogic.loadGame()
	boardView.gameLogic.win = boardView.win
	boardView.gameLogic.lose = boardView.lose

	computerMoveButton := widget.NewButton("PC Move", func() {
		boardView.computerMove()

	})

	newGameButton := widget.NewButton("New Game", func() {
		boardView.reloadGame(true)

	})

	instructionsMenu := fyne.NewMenu("Instructions",
		fyne.NewMenuItem("How to Play", func() { boardView.instructions() }),
		fyne.NewMenuItem("Wikipedia on NIM", func() {
			u, _ := url.Parse("https://en.wikipedia.org/wiki/Nim")
			_ = a.OpenURL(u)
		}))

	buttons := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), newGameButton, layout.NewSpacer(), computerMoveButton, layout.NewSpacer())

	boardViewRapper := container.New(layout.NewHBoxLayout(), layout.NewSpacer(), boardView, layout.NewSpacer())

	boardSizeItem := fyne.NewMenuItem("Change difficulty ", nil)
	boardImageItem := fyne.NewMenuItem("Change image", nil)
	boardSizeItem.ChildMenu = fyne.NewMenu("",
		fyne.NewMenuItem("Easy", func() { boardView.changeDifficulty(easyDimension) }),
		fyne.NewMenuItem("Medium", func() { boardView.changeDifficulty(medDimension) }),
		fyne.NewMenuItem("Hard", func() { boardView.changeDifficulty(hardDimension) }),
		fyne.NewMenuItem("Legendary", func() { boardView.changeDifficulty(legendaryDimension) }),
	)
	boardImageItem.ChildMenu = fyne.NewMenu("",
		fyne.NewMenuItem("Match Stick", func() { boardView.changeMatchImage(matchImageName) }),
		fyne.NewMenuItem("Tesla Logo", func() { boardView.changeMatchImage(TeslaLogoImageName) }),
		fyne.NewMenuItem("Tesla Battery", func() { boardView.changeMatchImage(teslaBatteryImageName) }),
	)
	customizeMenu := fyne.NewMenu("Customize", boardSizeItem, boardImageItem)

	//initialization of the main Menu
	topMainMenu := fyne.NewMainMenu(instructionsMenu, customizeMenu, aboutMenu)

	//Attaches the menu to the window
	w.SetMainMenu(topMainMenu)
	w.SetContent(container.New(layout.NewVBoxLayout(), InfoContainter, boardViewRapper, buttons))
	w.Resize(fyne.NewSize(1000, 800))
	w.ShowAndRun()
}
